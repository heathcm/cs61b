import static org.junit.Assert.*;

import com.google.common.collect.ComputationException;
import org.junit.Test;

public class CompoundInterestTest {

    @Test
    public void testNumYears() {
        /** Sample assert statement for comparing integers
        assertEquals(0, 0); */
        assertEquals(0, CompoundInterest.numYears(2021));
        assertEquals(1, CompoundInterest.numYears(2022));
        assertEquals(100, CompoundInterest.numYears(2121));

    }

    @Test
    public void testFutureValue() {
        double tolerance = 0.01;
        assertEquals(12.544, CompoundInterest.futureValue(10, 12, 2023), tolerance);
        assertEquals(25, CompoundInterest.futureValue(100, -50, 2023), tolerance);
        assertEquals(10, CompoundInterest.futureValue(10, 12, 2021), tolerance);
    }

    @Test
    public void testFutureValueReal() {
        double tolerance = 0.01;
        assertEquals(295712.29, CompoundInterest.futureValueReal(1000000, 0, 2061, 3), tolerance);
    }


    @Test
    public void testTotalSavings() {
        /*
         *  For example, if PERYEAR is 5000, TARGETYEAR is 2023, and RATE is 10,
         *  then the result will be 5000*1.1*1.1 + 5000*1.1 + 5000 =
         *  16550
         */
        double tolerance = 0.01;
        assertEquals(16550, CompoundInterest.totalSavings(5000, 2023, 10), tolerance);

    }

    @Test
    public void testTotalSavingsReal() {
        double tolerance = 0.01;
        assertEquals(16550, CompoundInterest.totalSavingsReal(5000, 2023, 10, 0), tolerance);

    }

    /* Run the unit tests in this file. */
    public static void main(String... args) {
        System.exit(ucb.junit.textui.runClasses(CompoundInterestTest.class));
    }
}
