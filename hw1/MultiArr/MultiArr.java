/** Multidimensional array 
 *  @author Zoe Plaxco
 */

public class MultiArr {

    /**
    {{“hello”,"you",”world”} ,{“how”,”are”,”you”}} prints:
    Rows: 2
    Columns: 3
    
    {{1,3,4},{1},{5,6,7,8},{7,9}} prints:
    Rows: 4
    Columns: 4
    */
    public static void printRowAndCol(int[][] arr) {
        int rows = arr.length;
        int cols = arr[0].length;
        for (int[] i:arr) {
            if(i.length>cols) {
                cols = i.length;
            }
        };
        System.out.printf("Rows: %d\nColumns: %d\n", rows, cols);
    } 

    /**
    @param arr: 2d array
    @return maximal value present anywhere in the 2d array
    */
    public static int maxValue(int[][] arr) {
        int max = arr[0][0];
        for (int[] i : arr){
            for (int j : i){
                if (j > max){
                    max = j;
                }
            }
        }
        return max;
    }

    /**Return an array where each element is the sum of the 
    corresponding row of the 2d array*/
    public static int[] allRowSums(int[][] arr) {
        int[] row_sum = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            for (int num : arr[i]){
                row_sum[i] += num;
            }
        }
        return row_sum;
    }
}