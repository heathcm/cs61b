import static org.junit.Assert.*;
import org.junit.Test;

public class MultiArrTest {

    @Test
    public void testMaxValue() {
        int[][] test = {{1,3,4},{-1},{5,6,10,8},{7,9}};
        assertEquals(10, MultiArr.maxValue(test));
    }

    @Test
    public void testAllRowSums() {
        int[][] test = {{1,3,4},{-1},{5,6,10,8},{7,9}};
        assertArrayEquals(new int[] {8, -1, 29, 16}, MultiArr.allRowSums(test));
    }

    /* Run the unit tests in this file. */
    public static void main(String... args) {
        System.exit(ucb.junit.textui.runClasses(MultiArrTest.class));
    }
}
